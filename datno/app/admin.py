from django.contrib import admin
from app.models import Item, Product, Employee

admin.site.register(Item)
admin.site.register(Product)
admin.site.register(Employee)
# Register your models here.
