# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=200)),
                ('product_description', models.CharField(max_length=200)),
                ('product_image', models.ImageField(upload_to=b'images')),
                ('product_price', models.DecimalField(max_digits=6, decimal_places=2)),
            ],
        ),
    ]
