# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_product'),
    ]

    operations = [
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('employee_name', models.CharField(max_length=200)),
                ('employee_title', models.CharField(max_length=200)),
                ('employee_background', models.CharField(max_length=200)),
                ('employee_picture', models.ImageField(upload_to=b'static/static-only/employee/images')),
            ],
        ),
        migrations.AlterField(
            model_name='product',
            name='product_image',
            field=models.ImageField(upload_to=b'static/static-only/product/images'),
        ),
    ]
