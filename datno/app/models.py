from django.db import models
from PIL import Image

class Item(models.Model):
    publish_date = models.DateField(max_length=200)
    name = models.CharField(max_length=200)
    detail = models.CharField(max_length=1000)
    thumbnail = models.CharField(max_length=200)

class Product(models.Model):
    product_name = models.CharField(max_length=200)
    product_description = models.CharField(max_length=200)
    product_image = models.ImageField(upload_to='static/static-only/product/images')
    product_price = models.DecimalField(max_digits=6, decimal_places=2)
    active = models.BooleanField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.product_name

class Employee(models.Model):
	employee_name = models.CharField(max_length=200)
	employee_title = models.CharField(max_length=200)
	employee_background = models.CharField(max_length=200)
	employee_picture = models.ImageField(upload_to='static/static-only/employee/images')