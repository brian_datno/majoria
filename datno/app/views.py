from django.shortcuts import render
import datetime
from .models import Item, Product

def index(request):
    product_list = Product.objects.order_by("-created_at")
    now = datetime.datetime.now()
    context = {'product_list' : product_list}
    return render(request, 'index.html', context)

def datapp(request):
    return render(request, "datno/datapp.html")