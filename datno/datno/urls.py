from django.conf.urls import include, url
from django.contrib import admin
from app import views

from django.conf import settings
from django.conf.urls.static import static 
from django.conf.urls.static import static
from django.conf import settings
from django.conf.urls import patterns

from django.contrib import admin 
admin.autodiscover()

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^datapp/', views.datapp, name='datapp'),
]
urlpatterns += patterns('', (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}))